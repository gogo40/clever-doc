Core Development
===========================================

The *Clever Team* is usually on-line at IRC #clever (irc.freenode.net)

-----------------
Coding standards
-----------------

We mostly code based on the *Google C++ Style Guide* [1], which seems sane
enough.
The *LLVM* [2] one is also quite nice.

.. epigraph::

  Use the .cc extension for C++ source files and .h for C++ headers,
  hopefully your compiler is smart enough to identify C++ source inside
  a .h (don't worry, GCC and Clang are!). Oh, I almost forgot, please
  use tabs instead of spaces, UTF-8 file encoding and UNIX file endings
  (Line feed a.k.a. \n).

If you don't know how to contribute, but know how to code, it'd be nice if you
could implement one of the ideas from our whishlist:
`TODO List & Ideas <https://github.com/clever-lang/clever/wiki/TODO-List-&-Ideas>`_

**Git help:** - `Fork a repo <http://help.github.com/fork-a-repo/>`_ - `Send pull requests <http://help.github.com/send-pull-requests/>`_

[1] - `Google C++ Style Guide <http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml>`_

[2] - `LLVM Coding Standards <http://llvm.org/docs/CodingStandards.html>`_
