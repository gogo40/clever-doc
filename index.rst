.. Clever documentation master file, created by
   sphinx-quickstart on Sun Mar  4 22:26:23 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Clever documentation
================================================

  .. image:: logo.jpg

Welcome to Clever documentation!

.. toctree::
   :maxdepth: 1

   getting_started
   packages/index
   faq
   core
   api/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

